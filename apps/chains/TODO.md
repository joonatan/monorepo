## Goals

- Learn tRPC
- Learn NextJs server side rendering (without the app directory)
- Learn react-aria
- Learn react-hook-form
- create Calendar () component with react-aria
  - date selector
  - showing a list of dates
  - custom styling using tailwind
- create reusable form components that are accessible
  - react-hook-form
  - react-aria

### later

- authjs setup (Discord / Google)
- planetscale database

## article options

- serverSideProps / tRPC
  - what is server side call, what is client side, when to use which
  - make a note about it
  - use prisma directly vs. api calls
- creating a calendar component from scracth
  - look-n-feel is fully customizable but it's accessible
- creating date selector component
- creating reusable input components with react-hook-form
- react-hook-form validation with zod
  - dunno if there is enough here?
    some small stuff like refinements and showing dynamic errors while writing
    larger stuff can be found in the HKI project like
    - validating interconnected dependencies (startDate / endDate)
    - customizing zod schema on runtime (passing parameters to refinements)

## TODO list

- Write a basic version that allows showing a calendar
- Allows posting a single note per calendar day
  - DONE / NOT
- TODO replace the API calls (on client) with server side prisma calls
- Create a list of tasks
- Calendar view for the X:s
- Possible for a user to add an Xs to a day (normal view only allows adding an X to today or yesterday).
- Allows creation of chains (name + calendar) per user
- Chains are by default private but a link can be shared (like Google Docks)
- Add daily notifications (either using a messager app or email)
  - sendinblue for emails
  - push notifcations if installed as a PWA (maybe also otherwise)
  - discord / slack / telegram notifcations

## Tech

- T3 stack
- Prisma
- AuthJs
- Next
- Planetscale DB
- Vercel

## React Libs

- react-aria
- react-hook-form
- zod (form validation)
- zod + tRPC (api calls)

## Data

- User
  - owns chains
  - date logged in
  - date last login
- Chain
  - name
  - description (more elaborate desc)
  - links to dates
  - goal interval X to Y (dates)
  - ran interval X to Y (dates)
  - read-access (SHARED / PRIVATE / PUBLIC)
  - slug (short url for sharing instead of id)
- Chain note (this is extra data per day)
  - percentage (0 - 100)
  - comment

Steps

- Draw the calendar view
- Allow user to start a chain (name the chain)
  - form
- Allow user to mark days in the chain (X for done)
- Deploy to Vercel (without database)
- Add database
- Add auth and user management
  - google auth only
