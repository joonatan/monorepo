import React from 'react'
import { signIn, signOut, useSession } from 'next-auth/react'
import { Layout as BaseLayout } from 'ui'

const links = [
  { label: 'Home', href: '/' },
  { label: 'Chains', href: '/chains' },
]

const Layout = ({
  title,
  children,
}: {
  title: string
  children: React.ReactNode
}) => {
  const { data: sessionData } = useSession()
  return (
    <BaseLayout
      title={title}
      links={[
        ...links,
        sessionData
          ? {
              type: 'button',
              label: 'Sign out',
              href: '',
              onPress: () => void signOut(),
            }
          : {
              type: 'button',
              label: 'Sign in',
              href: '',
              onPress: () => void signIn(),
            },
      ]}
    >
      {children}
    </BaseLayout>
  )
}

export default Layout
