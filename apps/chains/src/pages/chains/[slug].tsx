import { type PrismaClient } from 'chains-database'
import {
  type InferGetServerSidePropsType,
  type GetServerSidePropsContext,
  type NextPage,
} from 'next'
import { useRouter } from 'next/router'
import ChainPage from '~/components/ChainPage'
import { Button } from 'ui'
import Layout from '~/layouts/Layout'
import { prisma } from '~/server/db'
import { api } from '~/utils/api'
import { deserializeChain, serializeChain } from '~/utils/serializers'

// TODO figure out how to get data from prisma (that is where is prisma)
// on fastify it extendes the req e.g. ctx.req.prisma but here it doesn't
export const getServerSideProps = async (ctx: GetServerSidePropsContext) => {
  console.log('Doing a server side fetch')
  const { slug } = ctx.query
  if (typeof slug === 'string') {
    const chain = await prisma.chain.findUnique({ where: { slug } })
    return {
      props: {
        chain: chain != null ? serializeChain(chain) : null,
      },
    }
  } else if (Array.isArray(slug)) {
    // TESTING stuff
    console.log('slug is an array')
  }
  return {
    props: {
      chain: null,
    },
  }
}

// TODO replace the client side queries with server side
// So what we gonna do here is
// Get the initial version from prisma (server side)
// then we poll with tRPC on component rerender (and maybe timer)
// if there is something newer and replace it
// TEST case: use two browsers / windows to chains the data and see the chain happen on the other one
// Another option is to load the client side update after the initial render
// or do a periodic update (like every couple of seconds)
const Chain = ({
  chain,
}: InferGetServerSidePropsType<typeof getServerSideProps>) => {
  const { query, replace, asPath } = useRouter()

  console.log('query: ', query)
  if (query.client === 'true') {
    console.log('should use client side not server side')
  } else {
    console.log('use server side')
  }
  /* Client side version that should be used for updates
   * We need to do some conditional logic
   * if chain != null then
   * do a query (updatedAt > lastUpdate)
   * if (got data) { append to the chains }
   * setLastUpdate(now)
   */

  // TODO this should be conditional / in hook
  /*
  const slug =
    query.slug != null && typeof query.slug === 'string' ? query.slug : null

  // TODO error
  if (slug == null) {
    return null
  }

  const { data, isLoading } = api.chains.getBySlug.useQuery({ slug })

  if (isLoading == null) {
    return <div>Spinner</div>
  }
  */

  // TODO refactor these to util functions that replace query params
  // TODO don't remove the ? (but add it if it doesn't exist) because we mess other filter parameters
  // e.g. ?after="2021-03-21"&client=true&only-active=true
  const handleToServer = () => {
    void replace(asPath.replace('?client=true', '').concat('?client=false'))
  }
  const handleToClient = () => {
    void replace(asPath.replace('?client=false', '').concat('?client=true'))
  }
  const isClientSide = query.client === 'true'
  const toggleRendering = () =>
    isClientSide ? handleToServer() : handleToClient()

  return (
    <Layout title={`Chain ${chain?.slug ?? '???'}`}>
      <Button onPress={toggleRendering}>
        {isClientSide ? 'Use server' : 'Use client'}
      </Button>
      {chain != null && <ChainPage data={deserializeChain(chain)} />}
    </Layout>
  )
}

export default Chain
