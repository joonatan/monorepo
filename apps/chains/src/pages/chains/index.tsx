import type {
  GetServerSidePropsContext,
  InferGetServerSidePropsType,
} from 'next'
import { useState } from 'react'
import { zodResolver } from '@hookform/resolvers/zod'
import { CalendarDate } from '@internationalized/date'
import { Controller, useForm } from 'react-hook-form'
import { z } from 'zod'
import ChainCard from '~/components/ChainCard'
import { Button, Modal, TextInput, DatePicker, DateSchema } from 'ui'
import Layout from '~/layouts/Layout'
import { api } from '~/utils/api'
import { prisma } from '~/server/db'
import { deserializeChain, serializeChain } from '~/utils/serializers'

export const getServerSideProps = async (ctx: GetServerSidePropsContext) => {
  console.log('Doing a server side fetch')
  const { slug } = ctx.query
  if (typeof slug === 'string') {
    const chains = await prisma.chain.findMany({})
    return {
      props: {
        chains:
          chains != null ? chains.map((chain) => serializeChain(chain)) : null,
      },
    }
  } else if (Array.isArray(slug)) {
    // TESTING stuff
    console.log('slug is an array')
  }
  return {
    props: {
      chains: null,
    },
  }
}

const NewChainFormSchema = z.object({
  title: z.string().min(1),
  startDate: DateSchema,
  endDate: DateSchema.optional(),
  description: z.string().optional(),
  slug: z.string().optional(),
})
type NewChainFormType = z.infer<typeof NewChainFormSchema>

const TZ = 'Europe/Helsinki'

const NewChainForm = ({
  onDone,
}: {
  onDone: (shouldRefetch: boolean) => void
}) => {
  const { control, register, handleSubmit, getFieldState } =
    useForm<NewChainFormType>({
      resolver: zodResolver(NewChainFormSchema),
      defaultValues: {},
      reValidateMode: 'onChange',
    })

  const mutation = api.chains.createById.useMutation({
    onError: (err) => {
      console.error('failed mutation:', err)
    },
    onSuccess: (d) => {
      console.log('mutation success:', d)
      onDone(true)
    },
  })

  // we should end up here if the form is valid?
  const onSubmit = (data: NewChainFormType) => {
    // TODO generate the slug by using a API call or if user provided it validate that it's unique
    const genSlug = (title: string) => {
      return title.toLowerCase().replace(' ', '-')
    }
    // TODO add userId if the user is logged in

    const sd = data.startDate
    const ed = data.startDate
    const start = new CalendarDate(sd.year, sd.month, sd.day)
    const end = new CalendarDate(ed.year, ed.month, ed.day)
    mutation.mutate({
      ...data,
      startDate: start.toDate(TZ),
      endDate: end.toDate(TZ),
      slug: data.slug || genSlug(data.title),
    })
    onDone(true)
  }

  /// TODO need to input following
  /// title
  /// start date (today by default)
  /// end date (optional)
  /// description (optional)
  /// slug (optional)

  return (
    <form
      className="grid grid-cols-2 gap-4"
      onSubmit={handleSubmit(onSubmit)}
      noValidate
    >
      <h2 className="col-span-full text-xl">Create chain</h2>
      <TextInput
        {...register('title', { required: true })}
        label="Title"
        className="col-span-full"
        error={
          getFieldState('title').isTouched
            ? getFieldState('title').error?.message
            : undefined
        }
        required
      />

      <Controller
        control={control}
        name="startDate"
        rules={{ required: true }}
        render={({ field: { onChange } }) => (
          <DatePicker
            label="Start date"
            onChange={onChange}
            className="col-span-full"
          />
        )}
        /*
        error={
          getFieldState('startDate').isTouched
            ? getFieldState('startDate').error?.message
            : undefined
        }
        */
      />
      <Controller
        control={control}
        name="endDate"
        // rules={{ required: false }}
        render={({ field: { onChange } }) => (
          <DatePicker
            label="End date"
            onChange={onChange}
            className="col-span-full"
          />
        )}
      />
      <TextInput
        {...register('description')}
        label="Description"
        className="col-span-full"
        error={
          getFieldState('description').isTouched
            ? getFieldState('description').error?.message
            : undefined
        }
      />
      <TextInput
        {...register('slug')}
        label="Description"
        className="col-span-full"
        error={
          getFieldState('slug').isTouched
            ? getFieldState('slug').error?.message
            : undefined
        }
      />
      <Button primary type="submit" className="col-span-1">
        Create
      </Button>
      <Button onPress={() => onDone(false)} className="col-span-1">
        Cancel
      </Button>
    </form>
  )
}

const Chains = ({
  chains,
}: InferGetServerSidePropsType<typeof getServerSideProps>) => {
  const [isShowModal, setIsShowModal] = useState(false)
  const { data, refetch } = api.chains.getAll.useQuery()

  const handleNewClick = () => {
    setIsShowModal(true)
  }

  const handleDone = (shouldRefetch: boolean) => {
    setIsShowModal(false)
    if (shouldRefetch) {
      void refetch()
    }
  }

  // prefer client data, use server data till it's available
  const displayChains =
    data != null ? data : chains?.map((x) => deserializeChain(x))

  return (
    <Layout title="Chains index">
      <Button className="mb-8" onPress={handleNewClick}>
        Create new
      </Button>
      {/* eslint-disable-next-line tailwindcss/classnames-order, tailwindcss/no-custom-classname */}
      <div className="grid w-full grid-cols-auto gap-4">
        {displayChains?.map((x) => (
          <ChainCard key={x.id} data={x} />
        ))}
      </div>
      {isShowModal && (
        <Modal onClose={() => setIsShowModal(false)}>
          <NewChainForm onDone={handleDone} />
        </Modal>
      )}
    </Layout>
  )
}

export default Chains
