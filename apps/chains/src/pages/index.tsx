import { type NextPage } from 'next'
import { signIn, signOut, useSession } from 'next-auth/react'
import { Button } from 'ui'
import { api } from '~/utils/api'
import Layout from '~/layouts/Layout'

const Home: NextPage = () => {
  return (
    <Layout title="Chains">
      <main className="flex min-h-screen flex-col items-center justify-center">
        <AuthShowcase />
      </main>
    </Layout>
  )
}

export default Home

const AuthShowcase: React.FC = () => {
  const { data: sessionData } = useSession()

  return (
    <div className="flex flex-col items-center justify-center gap-4">
      <Button
        onPress={sessionData ? () => void signOut() : () => void signIn()}
      >
        {sessionData ? 'Sign out' : 'Sign in'}
      </Button>
    </div>
  )
}
