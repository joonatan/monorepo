import { z } from 'zod'

import {
  createTRPCRouter,
  publicProcedure,
  protectedProcedure,
} from '~/server/api/trpc'

// TODO should make protected after adding auth
export const chainsRouter = createTRPCRouter({
  getBySlug: publicProcedure
    .input(z.object({ slug: z.string() }))
    .query(({ ctx, input: { slug } }) => {
      return ctx.prisma.chain.findUnique({ where: { slug } })
    }),

  getById: publicProcedure
    .input(z.object({ id: z.string() }))
    .query(({ ctx, input: { id } }) => {
      return ctx.prisma.chain.findUnique({ where: { id } })
    }),

  getAll: publicProcedure.query(({ ctx }) => {
    return ctx.prisma.chain.findMany()
  }),

  createById: publicProcedure
    .input(
      z.object({
        title: z.string(),
        startDate: z.date(),
        endDate: z.date().optional(),
        description: z.string().optional(),
        slug: z.string(),
      }),
    )
    .mutation(({ ctx, input }) => {
      return ctx.prisma.chain.create({ data: input })
    }),

  getSecretMessage: protectedProcedure.query(() => {
    return 'you can now see this secret message!'
  }),
})
