import React from 'react'
import { dayDiff, printPeriod } from '~/utils/date'
import Link from 'next/link'
import { DateFormatter } from '@internationalized/date'

// TODO use prisma type or tRPC type or something
type DataProps = {
  title: string
  description?: string | null
  startDate: Date
  endDate?: Date | null
  slug?: string
  id: string
}

// TODO type the data that is passed here
// TODO use the grid to make this change orientation (horizontal / vertical layout)
// just for funzies not really useful in this case
// TODO make the header controllable (h2 / h3)
const ChainCard = ({ data }: { data: DataProps }) => {
  const { title, description, slug } = data
  // TODO fallback to id if slug doesn't exist
  const start = data.startDate
  const end = data.endDate
  const daysRun = dayDiff(start, new Date())

  const formatter = new DateFormatter('fi')
  return (
    <Link
      className="col-span-1 row-span-3 rounded-xl bg-slate-300 p-4 duration-200 ease-in hover:scale-105 dark:bg-slate-800"
      href={`/chains/${slug ?? ''}`}
    >
      <h2 className="col-span-full mb-4 text-xl">Chain: {title}</h2>
      <p className="mb-4">{description}</p>
      <p className="mb-4">TODO this should show total count / total days</p>
      <p className="mb-4">
        {formatter.format(start)}
        {`${end != null ? '-'.concat(formatter.format(end)) : ''}`}
      </p>
      <p className="mb-4">Started {printPeriod(daysRun)} ago</p>
    </Link>
  )
}

export default ChainCard
