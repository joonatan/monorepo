import React from 'react'
import { type Chain } from 'chains-database'
import { Button } from 'ui'
import { dayDiff, printPeriod } from '~/utils/date'
import { DateFormatter } from '@internationalized/date'

// TODO (this is in the page not here?) do a fetch with server component / getServerSideProps
// TODO pass the data from fetch here
// TODO use Prisma.Chain data type
const ChainPage = ({ data }: { data: Chain }) => {
  const { title, description } = data

  const start = data.startDate
  const end = data.endDate
  const daysRun = dayDiff(new Date(), start)

  const formatter = new DateFormatter('fi')

  const handleSave = () => {
    console.warn('handleSave: NOT implemented')
  }
  const handleDelete = () => {
    console.warn('handleDelete: NOT implemented')
  }

  return (
    <div>
      <Button className="col-span-1" onPress={handleSave}>
        TODO save button
      </Button>
      <Button className="col-span-1" onPress={handleDelete}>
        TODO delete button
      </Button>
      <h2 className="col-span-full mb-4 text-xl">Chain: {title}</h2>
      <p className="mb-4">{description}</p>
      <p className="mb-4">TODO this should show total count / total days</p>
      <p className="mb-4">
        {formatter.format(start)}
        {`${end != null ? '-'.concat(formatter.format(end)) : ''}`}
      </p>
      <p className="mb-4">Started {printPeriod(daysRun)} ago</p>
    </div>
  )
}

export default ChainPage
