type Period = {
  years: number
  months: number
  days: number
}

// TODO print format needs a bit more look into
export const printPeriod = (p: Period) => `
  ${p.years > 0 ? `${p.years} years` : ''}
  ${p.months > 0 ? `${p.months} months` : ''}
  ${p.days === 0 ? 'today' : ''}
  ${p.days === 1 ? 'a day' : ''}
  ${p.days > 1 ? `${p.days} days ` : ''}
`

export const dayDiff = (d1: Date, d2: Date) => {
  return {
    years: Math.abs(d1.getFullYear() - d2.getFullYear()),
    months: Math.abs(d1.getMonth() - d2.getMonth()),
    days: Math.abs(d1.getDate() - d2.getDate()),
  }
}
