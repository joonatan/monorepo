import { type Chain } from 'chains-database'

// this is silly but it's because Next hates Date
export const serializeChain = (chain: Chain) => ({
  ...chain,
  createdAt: chain.createdAt.toString(),
  updatedAt: chain.updatedAt.toString(),
  startDate: chain.startDate.toString(),
  endDate: chain.startDate.toString(),
})

// TODO could use infer_return_type<serializeChain>
type ChainStringDates = Omit<
  Chain,
  'createdAt' | 'updatedAt' | 'startDate' | 'endDate'
> & {
  createdAt: string
  updatedAt: string
  startDate: string
  endDate: string
}

export const deserializeChain = (chain: ChainStringDates) => ({
  ...chain,
  createdAt: new Date(chain.createdAt),
  updatedAt: new Date(chain.updatedAt),
  startDate: new Date(chain.startDate),
  endDate: new Date(chain.endDate),
})
