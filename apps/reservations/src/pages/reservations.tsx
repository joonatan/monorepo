import type { NextPage } from 'next'
import { Button } from 'ui'
import Layout from '~/layouts/Layout'
import { api } from '~/utils/api'

const Reservations: NextPage = () => {
  const res = api.reservations.getAllReservations.useQuery()

  if (res.isLoading || res.data == null) {
    return <div>Loading reservations</div>
  }
  if (res.isError) {
    return <div>ERROR: fetching reservations</div>
  }

  // TODO this should open up a modal (with the content of MakeReservation)
  const handleNew = () => {
    console.log('handle new')
  }

  return (
    <Layout title="reservations">
      <h2 className="mb-4 text-2xl">Reservations</h2>
      <div className="mb-4">Total count: {res.data.length}</div>
      <table className="w-full">
        <tr className="border-b-2 border-blue-500">
          <th className="text-center">date</th>
          <th className="text-center">time</th>
          <th className="text-right">name</th>
          <th className="text-right">email</th>
        </tr>
        {res.data.map((x) => (
          <tr key={x.id}>
            <td className="mx-4">{x.time.toLocaleDateString()}</td>
            <td className="mx-4">{x.time.toLocaleTimeString()}</td>
            <td className="mx-4 text-right">{x.user.name}</td>
            <td className="mx-4 text-right">{x.user.email}</td>
          </tr>
        ))}
      </table>
      <Button className="my-4" onPress={handleNew} isDisabled={true}>
        New reservation
      </Button>
    </Layout>
  )
}

export default Reservations
