import { useState, type Key } from 'react'
import { Item } from 'react-stately'
import { toast } from 'react-toastify'
import { isSameDay, CalendarDate } from '@internationalized/date'
import type { DateValue } from '@internationalized/date'
import type { Service } from 'reservations-database'
import { DatePicker, TimeField, ListBox } from 'ui'
import { api } from '~/utils/api'
import ServiceCard from '~/components/ServiceCard'
import CustomerForm from '~/components/CustomerForm'

const slotSize = 0.5

const TZ = 'Europe/Helsinki'

const toTimeSlots = (start: number, end: number) =>
  Array.from(Array(Math.ceil(end - start) / slotSize).keys()).map((index) => ({
    available: true,
    start: index * slotSize + start,
    end: (index + 1) * slotSize + start,
  }))

// TODO add a field to select it and a widget to the side to describe it
function SelectService({
  onChange,
  selected,
}: {
  onChange: (service: Service | null) => void
  selected: Service | null
}) {
  const services = api.reservations.getAllServices.useQuery()

  const handleSelected = (keys: 'all' | Set<Key>) => {
    console.log('service selected: ', keys)
    if (keys === 'all') {
      console.log('All selected')
    } else if (keys.size > 1) {
      console.error('Multi service selection is not allowed')
    } else {
      keys.forEach((x) => {
        const fser = services?.data?.filter((a) => a.id === x.valueOf())
        const ser = fser != null && fser.length > 0 ? fser[0] : null
        console.log('key: ', x, 'service: ', ser)
        onChange(ser ?? null)
      })
    }
  }

  if (services.isLoading || services.data == null) {
    return <div>Loading: available services</div>
  }
  if (services.isError) {
    return <div>ERROR: fetching services</div>
  }

  return (
    <div className="grid grid-cols-2">
      <div className="cols-span-1">
        <ListBox
          label="Service"
          selectionMode="single"
          onSelectionChange={handleSelected}
        >
          {services.data.map((x) => (
            <Item key={x.id}>{x.name}</Item>
          ))}
        </ListBox>
      </div>
      {selected && <ServiceCard service={selected} />}
    </div>
  )
}
function SelectReservationTime({
  date,
  service,
  onChange,
}: {
  date: Date
  service: Service
  onChange: (startTime: number) => void
}) {
  const res = api.reservations.getReservationsByDate.useQuery({ date })
  const workTimes = api.reservations.getWorkTimeByDate.useQuery({ date })

  if (res.isLoading || res.data == null) {
    return <div>Loading</div>
  }
  if (res.isError) {
    return <div>ERROR: fetching reservations</div>
  }

  if (workTimes.isLoading || workTimes.data == null) {
    return <div>Loading: available times for day: {date.toDateString()}</div>
  }
  if (res.isError) {
    return <div>ERROR: fetching work times for day: {date.toDateString()}</div>
  }

  // TODO remove defaults (use errors instead)
  const startTime =
    workTimes.data != null && workTimes.data.length > 0
      ? workTimes?.data[0]?.start?.getHours()
      : null
  const endTime =
    workTimes.data != null && workTimes.data.length > 0
      ? workTimes?.data[0]?.end?.getHours()
      : null

  // const totalTime = service.beforeTime + service.time + service.afterTime

  const handleTimeChanged = (keys: 'all' | Set<Key>) => {
    if (keys === 'all') {
      console.error('Multi service selection is not allowed')
    } else if (keys.size > 1) {
      console.error('Multi service selection is not allowed')
    } else {
      keys.forEach((x) => {
        if (typeof x.valueOf() === 'number') {
          onChange(x.valueOf() as number)
        } else {
          onChange(Number(x.valueOf()))
        }
      })
    }
  }

  if (startTime == null || endTime == null) {
    return <div>Incorrect start or end time for time selection</div>
  }

  /* TODO this widget requires a rework
   * Either use a popover select or Calendar look (one day column from 9 - 19, clickable, automatically moves the whole event)
   * use a select with a popover instead of a large list
   * show the start - end time (not just the start time)
   * TODO print in time format
   * TODO disabled colour (and some test data)
   * TODO if we can show the whole list? similarly to a Calendar app
   */
  return (
    <>
      <ListBox
        label="Time"
        selectionMode="single"
        onSelectionChange={handleTimeChanged}
      >
        {toTimeSlots(startTime, endTime).map((x) => (
          <Item key={x.start}>{String(x.start)}</Item>
        ))}
      </ListBox>
    </>
  )
}

function MakeReservation() {
  const res = api.reservations.getAllReservations.useQuery()
  const workTimes = api.reservations.getAllWorkTimes.useQuery()
  const mutateReservation = api.reservations.makeReservation.useMutation()

  const [selectedDate, setSelectedDate] = useState<DateValue | null>(null)
  const [selectedService, setSelectedService] = useState<Service | null>(null)
  // TODO proper time format (Range for the start, end of the service etc.)
  const [selectedTime, setSelectedTime] = useState<number | null>(null)

  // TODO pass the work calendar (filtered with reservations here)
  // TODO all of this should obviously be on higher level
  const isDateUnavailable = (d: DateValue) => {
    if (workTimes.data == null) {
      return true
    }

    // TODO this needs to use the service length also
    // TODO this needs to include reservations
    // TODO this should really be in the backend
    const ftimes = workTimes.data.filter((t) => {
      const s1 = new CalendarDate(
        t.start.getFullYear(),
        t.start.getMonth() + 1,
        t.start.getDate(),
      )
      return isSameDay(d, s1)
    })
    return ftimes.length === 0
  }

  const handleDateSelected = (date: DateValue) => {
    console.log('date selected: ', date)
    // TODO pull the current date info and show it as a widget to user
    setSelectedDate(date)
    toast.info(`date selected: ${date.toDate(TZ).toDateString()}`)
  }

  const makeReservation = () => {
    console.log('make reservation data: ', {
      service: selectedService,
      date: selectedDate,
      time: selectedTime,
    })
    if (
      selectedDate != null &&
      selectedTime != null &&
      selectedService != null
    ) {
      // FIXME the time needs to be set to the date
      const hours = Math.floor(selectedTime)
      const mins = (selectedTime - hours) * 60

      const d = selectedDate.set({ hour: hours, minute: mins })
      mutateReservation.mutate({
        date: d.toDate(TZ),
        service: { id: selectedService.id },
        customer: {
          email: 'alice@fake.fi',
          dog: 'TODO test dog',
          description: 'TODO form is not implemented',
          name: 'TODO name',
        },
      })
    }
  }

  const handleSubmit = (vals: {
    name: string
    email: string
    dog: string
    description?: string
  }) => {
    console.log('submitted: ', vals)
  }

  if (res.isError) {
    return <div>ERROR: loading reservations {res.error.message}</div>
  }
  if (res.isLoading || res.data == null) {
    return <div>Loading reservations</div>
  }

  // TODO make a form for the user info after time selection
  // TODO show progress bar for the reservation steps
  // - (select service)
  // - (select date)
  // - (select time)
  // - (select fill customer info)
  return (
    <div className="container">
      <h2 className="mb-4 text-2xl">Make a reservation</h2>
      <SelectService onChange={setSelectedService} selected={selectedService} />
      {selectedService != null && (
        <DatePicker
          onChange={handleDateSelected}
          isDateUnavailable={isDateUnavailable}
          aria-label="Date"
        />
      )}
      {/*<TimeField locale="fi" aria-label="Aika" />*/}
      {/* TODO should show disabled Selection component as a skeleton */}
      {selectedDate != null && selectedService != null ? (
        <SelectReservationTime
          date={selectedDate.toDate(TZ)}
          service={selectedService}
          onChange={setSelectedTime}
        />
      ) : (
        <div>Select a date and service first</div>
      )}
      <CustomerForm
        className="my-8"
        onDone={makeReservation}
        isDisabled={
          selectedDate == null ||
          selectedService == null ||
          selectedTime == null
        }
      />
    </div>
  )
}

export default MakeReservation
