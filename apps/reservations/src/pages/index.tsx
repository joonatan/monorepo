import { type NextPage } from 'next'

import Layout from '~/layouts/Layout'
import MakeReservation from './MakeReservation'

const Home: NextPage = () => {
  return (
    <Layout title="Reservations">
      <MakeReservation />
    </Layout>
  )
}

export default Home
