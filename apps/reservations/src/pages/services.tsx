import type { NextPage } from 'next'
import { Button } from 'ui'
import Layout from '~/layouts/Layout'
import ServiceCard from '../components/ServiceCard'
import { api } from '~/utils/api'

const Services: NextPage = () => {
  const services = api.reservations.getAllServices.useQuery()

  if (services.isLoading || services.data == null) {
    return <div>Loading: available services</div>
  }
  if (services.isError) {
    return <div>ERROR: fetching services</div>
  }

  const handleNew = () => {
    console.log('handle new')
  }

  return (
    <Layout title="services">
      <h2 className="my-8 text-3xl">All services</h2>
      <div className="my-4 grid grid-cols-1 gap-4 md:grid-cols-2 lg:grid-cols-4">
        {services.data.map((x) => (
          <ServiceCard key={x.id} service={x} />
        ))}
      </div>
      <Button onPress={handleNew}>New service</Button>
    </Layout>
  )
}

export default Services
