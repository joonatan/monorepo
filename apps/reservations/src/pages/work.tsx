import type { NextPage } from 'next'
import { useState } from 'react'
import { type WorkTime } from 'reservations-database'
import {
  Button,
  DatePicker,
  DateSchema,
  Modal,
  TimeInput,
  TimeSchema,
} from 'ui'
import { Controller, useForm } from 'react-hook-form'
import { z } from 'zod'
import { zodResolver } from '@hookform/resolvers/zod'
import Layout from '~/layouts/Layout'
import { api } from '~/utils/api'
import { CalendarDateTime, today } from '@internationalized/date'
import { toast } from 'react-toastify'

const TZ = 'Europe/Helsinki'

const WorkTimeFormSchema = z.object({
  date: DateSchema,
  worker: z.string().cuid(),
  // TODO should have optional id for edits
  // id: z.string().uuid().optional(),
  startTime: TimeSchema,
  endTime: TimeSchema,
})
// TODO refinements (after errors are properly displayed)
// date >= today
// startTime < endTime

type WorkTimeForm = z.infer<typeof WorkTimeFormSchema>

// Modal inner
// TODO allow passing initial values so we can use this as edit form
const NewWorkTime = ({
  onSuccess,
  workerId,
}: {
  onSuccess: () => void
  workerId: string
}) => {
  const { control, register, handleSubmit } = useForm<WorkTimeForm>({
    resolver: zodResolver(WorkTimeFormSchema),
    defaultValues: {},
    // don't revalidate before submit because we use react-aria
    // reValidateMode: 'onChange',
  })

  const mutation = api.reservations.addWorkTime.useMutation({
    onSuccess: () => {
      toast.success('Created new work time')
      onSuccess()
    },
    onError: (error) => {
      console.error('Error: ', error)
      toast.error('Failed to create new work time')
    },
  })

  const onSubmit = (vals: WorkTimeForm) => {
    const d = vals.date
    const et = vals.endTime
    const st = vals.startTime
    const end = new CalendarDateTime(d.year, d.month, d.day, et.hour, et.minute)
    const start = new CalendarDateTime(
      d.year,
      d.month,
      d.day,
      st.hour,
      st.minute,
    )

    mutation.mutate({
      worker: vals.worker,
      end: end.toDate(TZ),
      start: start.toDate(TZ),
    })
  }

  return (
    <form
      onSubmit={handleSubmit(onSubmit)}
      className="grid grid-cols-auto gap-4"
    >
      <h2 className="col-span-full text-center text-3xl">New work time</h2>
      {/* TODO errors? / required? */}
      <Controller
        control={control}
        name="date"
        rules={{ required: true }}
        render={({ field: { onChange } }) => (
          <DatePicker label="Date" onChange={onChange} minValue={today(TZ)} />
        )}
      />
      <Controller
        control={control}
        name="startTime"
        rules={{ required: true }}
        render={({ field: { onChange } }) => (
          <TimeInput label="Start" onChange={onChange} />
        )}
      />
      <Controller
        control={control}
        name="endTime"
        rules={{ required: true }}
        render={({ field: { onChange } }) => (
          <TimeInput label="End" onChange={onChange} />
        )}
      />
      <input
        {...register('worker')}
        type="hidden"
        value={workerId}
        name="worker"
      />
      <Button type="submit">Add</Button>
    </form>
  )
}

const WorkTimeHeader = () => (
  <tr className="border-b dark:border-slate-800">
    <th className="px-4 py-2">paiva</th>
    <th className="px-4 py-2">pvm</th>
    <th className="px-4 py-2">aika</th>
    <th className="px-4 py-2">muuta</th>
    <th className="px-4 py-2">poista</th>
  </tr>
)

const WorkTimeRow = ({
  data,
  onDelete,
  onEdit,
}: {
  data: WorkTime
  onEdit: (x: WorkTime) => void
  onDelete: (x: WorkTime) => void
}) => {
  const formatter = new Intl.DateTimeFormat('FI', { timeStyle: 'short' })
  return (
    <tr
      className={`${new Date() < data.start ? '' : 'text-slate-500'} ${
        new Date() <= data.start && data.end < new Date() ? 'bg-yellow-800' : ''
      }`}
    >
      <td className="px-4 py-2">
        {data.start.toLocaleDateString('FI', { weekday: 'long' })}
      </td>
      <td className="px-4 py-2 text-right">
        {data.start.toLocaleDateString('FI')}
      </td>
      <td className="px-4 py-2 text-right">
        {formatter.format(data.start)} - {formatter.format(data.end)}
      </td>
      <td className="px-4 py-2 text-center">
        <button
          className="px-2 underline hover:bg-neutral-200"
          onClick={() => onEdit(data)}
        >
          Edit
        </button>
      </td>
      <td className="px-4 py-2 text-center text-red-600">
        <button
          className="px-2 underline hover:bg-neutral-200"
          onClick={() => onDelete(data)}
        >
          X
        </button>
      </td>
    </tr>
  )
}

const WorkTimes = () => {
  const [showEditDialog, setShowEditDialog] = useState(false)
  const workTimes = api.reservations.getAllWorkTimes.useQuery()

  // TODO move upwards (allow selecting the worker and every one of them has a separate schedule)
  const { data, isLoading } = api.reservations.getFirstWorker.useQuery()

  if (workTimes.isError) {
    return <div>Error loading work times</div>
  }
  if (workTimes.isLoading || isLoading) {
    return <div>Loading work times</div>
  }
  if (workTimes.data == null) {
    return <div>No work times</div>
  }

  const handleNew = () => {
    setShowEditDialog(true)
  }

  const handleEdit = (x: WorkTime) => {
    console.log('handle edit: NOT implemented')
    // TODO show edit dialog and set the edited values (where to query the values?)
  }
  const handleDelete = (x: WorkTime) => {
    console.log('handle delete: NOT implemented')
  }

  // TODO pass the worker id here (only showing one)
  // show the name at the top though (so we can do multi selection if needed)

  // TODO refactor this
  // TODO show a table on desktops
  // TODO allow date filtering (range)
  // TODO use the fi locale for times
  // TODO allow editing
  // TODO show upcoming / already gone with different colour
  return (
    <>
      <h2 className="mb-4 text-2xl">Work times</h2>
      <div>TODO filter widget</div>
      <div>
        TODO a threeways selector to show only past, the future, or both
        <span className="text-red-700"> Two toggles</span>
      </div>
      <div className="mb-4">Total work days: {workTimes.data.length}</div>
      <table className="rounded-md bg-slate-300 dark:bg-slate-900">
        <thead>
          <WorkTimeHeader />
        </thead>
        <tbody>
          {workTimes.data.map((x) => (
            <WorkTimeRow
              key={x.id}
              data={x}
              onDelete={handleDelete}
              onEdit={handleEdit}
            />
          ))}
        </tbody>
      </table>
      <div className="my-4 flex gap-4">
        <Button onPress={() => void workTimes.refetch()}>Refetch</Button>
        <Button
          onPress={handleNew}
          className="rounded-full bg-orange-500 px-8 py-2 text-slate-900"
        >
          New work time
        </Button>
      </div>
      {showEditDialog && (
        <Modal onClose={() => setShowEditDialog(false)}>
          <NewWorkTime
            onSuccess={() => {
              setShowEditDialog(false)
              workTimes.refetch()
            }}
            workerId={data?.id ?? ''}
          />
        </Modal>
      )}
    </>
  )
}

const Services: NextPage = () => {
  const workTimes = api.reservations.getAllWorkTimes.useQuery()

  if (workTimes.isLoading || workTimes.data == null) {
    return <div>Loading: available services</div>
  }
  if (workTimes.isError) {
    return <div>ERROR: fetching services</div>
  }

  return (
    <Layout title="Work times">
      <WorkTimes />
    </Layout>
  )
}

export default Services
