import { useForm } from 'react-hook-form'
import { z } from 'zod'
import { zodResolver } from '@hookform/resolvers/zod'
import { Button, TextInput } from 'ui'

const CustomerFormSchema = z.object({
  email: z.string().min(1),
  name: z.string().min(1),
  dog: z.string().min(1),
  description: z.string().optional(),
  // TODO add date and time (using Date Selector)
  // TODO add service (needs to be service uuid)
  // service: z.string().min(1),
})
type CustomerFormType = z.infer<typeof CustomerFormSchema>

const CustomerForm = ({
  onDone,
  className,
  isDisabled,
}: {
  onDone?: () => void
  className?: string
  isDisabled?: boolean
}) => {
  const { register, handleSubmit, getFieldState } = useForm<CustomerFormType>({
    resolver: zodResolver(CustomerFormSchema),
    defaultValues: {},
    reValidateMode: 'onChange',
  })

  const onSubmit = (data: CustomerFormType) => {
    console.log('submitted: ', data)
    if (onDone) {
      onDone()
    }
  }

  // TODO styling
  // TODO use react-aria
  // TODO description should be a textbox
  return (
    <form
      className="grid grid-cols-2 gap-4"
      onSubmit={handleSubmit(onSubmit)}
      noValidate
    >
      <h2 className="col-span-full text-xl">Create chain</h2>
      <TextInput
        {...register('email', { required: true })}
        label="Email"
        className="col-span-full"
        error={
          getFieldState('email').isTouched
            ? getFieldState('email').error?.message
            : undefined
        }
        required
      />
      <TextInput
        {...register('name', { required: true })}
        label="Name"
        className="col-span-full"
        error={
          getFieldState('name').isTouched
            ? getFieldState('name').error?.message
            : undefined
        }
        required
      />
      <TextInput
        {...register('dog', { required: true })}
        label="Dog"
        className="col-span-full"
        error={
          getFieldState('dog').isTouched
            ? getFieldState('dog').error?.message
            : undefined
        }
        required
      />
      <TextInput
        {...register('description', { required: true })}
        label="Additional information"
        className="col-span-full"
        error={
          getFieldState('description').isTouched
            ? getFieldState('description').error?.message
            : undefined
        }
        required
      />
      <Button primary isDisabled={isDisabled ?? true} type="submit">
        Submit
      </Button>
    </form>
  )
}

export default CustomerForm
