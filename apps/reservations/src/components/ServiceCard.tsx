import type { Service } from 'reservations-database'

function ServiceCard({ service }: { service: Service }) {
  return (
    <div className="rounded-md bg-slate-100 p-4 dark:bg-slate-800">
      <div>{service.name}</div>
      <div>time {service.time} min</div>
      <div>{service.price} €</div>
      <div>
        total slot size: {service.time + service.afterTime + service.beforeTime}{' '}
        min
      </div>
    </div>
  )
}

export default ServiceCard
