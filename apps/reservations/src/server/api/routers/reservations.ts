import { z } from 'zod'

import { createTRPCRouter, publicProcedure } from '../trpc'

export const reservationsRouter = createTRPCRouter({
  makeReservation: publicProcedure
    .input(
      z.object({
        customer: z.object({
          email: z.string(),
          name: z.string(),
          dog: z.string(),
          description: z.string(),
        }),
        service: z.object({ id: z.string() }),
        date: z.date(),
      }),
    )
    .mutation(({ ctx, input }) => {
      // TODO validate that it doesn't overlap with another reservation
      return ctx.prisma.reservation.create({
        data: {
          user: {
            connectOrCreate: {
              where: {
                email: input.customer.email,
              },
              create: {
                email: input.customer.email,
                name: input.customer.name,
              },
            },
          },
          dog: input.customer.dog,
          description: input.customer.description,
          service: {
            connect: {
              id: input.service.id,
            },
          },
          time: input.date,
        },
      })
    }),
  getAllReservations: publicProcedure.query(({ ctx }) => {
    // TODO populate the username / email here
    return ctx.prisma.reservation.findMany({
      include: {
        user: {
          select: { email: true, name: true },
        },
      },
    })
  }),
  getWorkTimeByDate: publicProcedure
    .input(z.object({ date: z.date() }))
    .query(({ ctx, input }) => {
      const d = input.date
      const gte = new Date(d.getFullYear(), d.getMonth(), d.getDate(), 0, 0)
      const lte = new Date(d.getFullYear(), d.getMonth(), d.getDate(), 23, 59)
      return ctx.prisma.workTime.findMany({
        where: {
          start: {
            gte,
            lte,
          },
        },
      })
    }),
  getReservationsByDate: publicProcedure
    .input(z.object({ date: z.date() }))
    .query(({ ctx, input }) => {
      const d = input.date
      const gte = new Date(d.getFullYear(), d.getMonth(), d.getDate(), 0, 0)
      const lte = new Date(d.getFullYear(), d.getMonth(), d.getDate(), 23, 59)
      return ctx.prisma.reservation.findMany({
        where: {
          time: {
            gte,
            lte,
          },
        },
      })
    }),
  addWorkTime: publicProcedure
    .input(
      z.object({ start: z.date(), end: z.date(), worker: z.string().cuid() }),
    )
    .mutation(({ ctx, input }) => {
      return ctx.prisma.workTime.create({
        data: {
          ...input,
          worker: {
            connect: {
              id: input.worker,
            },
          },
        },
      })
    }),
  getAllWorkTimes: publicProcedure.query(({ ctx }) => {
    return ctx.prisma.workTime.findMany({
      orderBy: {
        start: 'asc',
      },
    })
  }),
  // TODO allow disabling a service
  getAllServices: publicProcedure.query(({ ctx }) => {
    return ctx.prisma.service.findMany()
  }),
  getWorkers: publicProcedure.query(({ ctx }) => {
    return ctx.prisma.worker.findMany()
  }),
  getFirstWorker: publicProcedure.query(async ({ ctx }) => {
    const res = await ctx.prisma.worker.findMany({
      take: 1,
    })
    return res.find(() => true)
  }),
  // TODO this needs to filter the work time by reservations
  /*
  getAvailableTimes: publicProcedure.query(({ ctx }) => {
    return ctx.prisma.workTime.findMany()
  }),
  */
})
