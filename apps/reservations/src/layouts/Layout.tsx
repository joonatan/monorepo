import { Layout as BaseLayout } from 'ui'
import React from 'react'

const links = [
  { href: '/', label: 'Home' },
  {
    href: '/services',
    label: 'Services',
  },
  { href: '/work', label: 'Work time' },
  {
    href: '/reservations',
    label: 'Reservations',
  },
]

const Layout = ({
  title,
  children,
}: {
  title: string
  children: React.ReactNode
}) => (
  <BaseLayout title={title} links={links}>
    {children}
  </BaseLayout>
)

export default Layout
