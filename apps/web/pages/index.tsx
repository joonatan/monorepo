import type { NextPage } from 'next/types'
import { Button, Calendar, DatePicker } from 'ui'
import { Layout } from 'ui'
// TODO this should be abstracted (export it from ui instead)
import { SSRProvider } from 'react-aria'

const LocalComponent = ({ className }: { className?: string }) => (
  <div className={`${className} text-xl text-red-700`}>
    Testing local Tailwind
  </div>
)

const Home: NextPage = () => {
  const links = [
    { label: 'Home', href: '/' },
    { label: 'Chains', href: '/chains' },
    { label: 'Login', href: '/login' },
  ]

  const handleClick = () => console.log('click')

  return (
    <SSRProvider>
      <Layout title={'My website'} links={links}>
        <main className="grid grid-cols-auto-fill gap-4">
          <div className="cols-span-full row-span-1">
            <Button className="col-span-1" onPress={handleClick}>
              Bleep Blop
            </Button>
          </div>
          <Calendar className="col-span-1 col-start-1" />
          <LocalComponent className="col-start-1" />
          <div>
            Test DatePicker
            <DatePicker aria-label="Pick date" />
          </div>
        </main>
      </Layout>
    </SSRProvider>
  )
}

export default Home
