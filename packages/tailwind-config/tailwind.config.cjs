/** @type {import('tailwindcss').Config} */
const config = {
  content: [
    '../../packages/ui/**/**/*.{ts,tsx}',
    './src/**/*.{ts,tsx}',
    './**/*.{ts,tsx}'
  ],
  theme: {
    extend: {
      gridTemplateRows: {
        layout: '4rem 1fr 8rem',
        12: 'repeat(12, 1fr)',
      },
      gridTemplateColumns: {
        auto: 'repeat(auto-fit, minmax(15rem, 1fr))',
        'auto-fit': 'repeat(auto-fit, minmax(15rem, 1fr))',
        'auto-fill': 'repeat(auto-fill, minmax(15rem, 1fr))',
      },
    },
  },
  plugins: [],
}

module.exports = config
