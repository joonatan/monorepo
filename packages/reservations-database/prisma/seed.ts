import { PrismaClient } from 'reservations-database'

const prisma = new PrismaClient()

async function main(): Promise<void> {
  const alice = await prisma.user.upsert({
    where: { email: 'alice@fake.fi' },
    update: {},
    create: {
      email: 'alice@fake.fi',
      name: 'Alice',
    },
  })

  const roy = await prisma.user.upsert({
    where: { email: 'roy@fake.fi' },
    update: {},
    create: {
      email: 'roy@fake.fi',
      name: 'Alice',
    },
  })

  const worker = await prisma.worker.upsert({
    where: { email: 'worker@fake.fi' },
    update: {},
    create: {
      email: 'worker@fake.fi',
      name: 'Sam the Worker',
    },
  })

  await prisma.service.upsert({
    where: { name: 'hieronta' },
    update: {},
    create: {
      name: 'hieronta',
      time: 90,
      beforeTime: 15,
      afterTime: 15,
      price: 60,
    },
  })

  await prisma.service.upsert({
    where: { name: 'laserointi' },
    update: {},
    create: {
      name: 'laserointi',
      time: 20,
      beforeTime: 10,
      afterTime: 0,
      price: 30,
    },
  })

  await prisma.service.upsert({
    where: { name: 'hieronta (2 koiraa)' },
    update: {},
    create: {
      name: 'hieronta (2 koiraa)',
      time: 145,
      beforeTime: 15,
      afterTime: 15,
      price: 100,
    },
  })

  const today = new Date()
  const dates = Array.from(Array(5).keys()).map((index) => {
    const startTime = new Date(today.getFullYear(), today.getMonth(), today.getDate() + 7 + index, 9, 0)
    const endTime = new Date(today.getFullYear(), today.getMonth(), today.getDate() + 7 + index, 17, 0)
    return {
      start: startTime,
      end: endTime,
    }
  })

  await prisma.workTime.deleteMany({})
  await prisma.workTime.createMany({
    data: dates.map((x) => ({
      start: x.start,
      end: x.end,
      workerId: worker.id,
    })),
  })
  // TODO create some Vaction days / (and few hour intervals)
  // TODO create some reservations
}

main()
  .then(async () => {
    await prisma.$disconnect()
  })

  .catch(async (e) => {
    console.error(e)
    await prisma.$disconnect()
    process.exit(1)
  })
