import { PrismaClient } from 'chains-database'

const prisma = new PrismaClient()

async function main() {
  await prisma.chain.upsert({
    where: { slug: 'minimal' },
    update: {},
    create: {
      title: 'Minimal',
      slug: 'minimal',
      startDate: new Date(),
      description: 'testing bar',
    },
  })

  await prisma.chain.upsert({
    where: { slug: 'foobar' },
    update: {},
    create: {
      title: 'Foobar',
      slug: 'foobar',
      startDate: new Date(),
      description: 'testing foo',
    },
  })

  await prisma.chain.upsert({
    where: { slug: 'baz' },
    update: {},
    create: {
      title: 'Testing Baz',
      slug: 'baz',
      startDate: new Date(),
      description: 'testing foo',
    },
  })
}
main()
  .then(async () => {
    await prisma.$disconnect()
  })
  .catch(async (e) => {
    console.error(e)
    await prisma.$disconnect()
    process.exit(1)
  })
