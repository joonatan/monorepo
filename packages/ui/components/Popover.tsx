import React, { type RefObject, useRef } from 'react'
import { type AriaPopoverProps, DismissButton, usePopover } from 'react-aria'
import { type OverlayTriggerState } from 'react-stately'

function Popover({
  children,
  state,
  ...props
}: {
  children: React.ReactNode
  state: OverlayTriggerState
} & Omit<AriaPopoverProps, 'popoverRef'>) {
  const popoverRef: RefObject<HTMLDivElement> = useRef<HTMLDivElement>(null)
  const { popoverProps, underlayProps } = usePopover(
    {
      ...props,
      popoverRef,
    },
    state
  )

  // <Overlay> just doesn't do anything (it's not visible after click etc.)
  return (
    <>
      <div
        {...underlayProps}
        className="fixed inset-0 backdrop-brightness-50"
      />
      <div
        {...popoverProps}
        ref={popoverRef}
        className="absolute z-50 mt-2 rounded-md border border-gray-300 bg-white p-8 shadow-lg"
      >
        <DismissButton onDismiss={state.close} />
        {children}
        <DismissButton onDismiss={state.close} />
      </div>
    </>
  )
}

export default Popover
