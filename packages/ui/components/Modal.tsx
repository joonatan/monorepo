import React from 'react'

// TODO add focus trap
const Modal = ({
  children,
  onClose,
}: {
  children: React.ReactNode
  onClose: () => void
}) => {
  return (
    <div className="absolute left-0 top-0 z-50 h-screen w-screen ">
      <div className="relative h-1/2 w-1/2 translate-x-1/2 translate-y-1/2 rounded-xl bg-slate-300 p-4 backdrop-opacity-50 dark:bg-slate-800">
        <button
          className="absolute right-2 top-2 text-2xl font-bold"
          type="button"
          onClick={onClose}
        >
          X
        </button>
        {children}
      </div>
    </div>
  )
}

export default Modal
