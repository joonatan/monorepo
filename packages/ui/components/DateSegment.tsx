import React, { type RefObject, useRef } from 'react'
import { useDateSegment } from '@react-aria/datepicker'
import { type DateSegment, type DateFieldState } from 'react-stately'

function DateSegmentComp({
  segment,
  state,
}: {
  segment: DateSegment
  state: DateFieldState
}) {
  const ref: RefObject<HTMLDivElement> = useRef<HTMLDivElement>(null)
  const { segmentProps } = useDateSegment(segment, state, ref)

  return (
    <div
      {...segmentProps}
      ref={ref}
      style={{
        ...segmentProps.style,
        minWidth:
          segment.maxValue != null
            ? String(String(segment.maxValue).length) + 'ch'
            : undefined,
      }}
      className={`group box-content rounded-sm px-0.5 text-right tabular-nums outline-none focus:bg-violet-600 focus:text-white ${
        !segment.isEditable ? 'text-slate-500' : 'text-slate-800'
      }`}
    >
      {segment.text}
    </div>
  )
}

export default DateSegmentComp
