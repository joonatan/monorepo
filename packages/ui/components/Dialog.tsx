import React, { type RefObject, useRef } from 'react'
import { type AriaDialogProps, useDialog } from 'react-aria'

function Dialog({
  title,
  children,
  ...props
}: { title?: string; children: React.ReactNode } & AriaDialogProps) {
  const ref: RefObject<HTMLDivElement> = useRef<HTMLDivElement>(null)
  const { dialogProps, titleProps } = useDialog(props, ref)

  return (
    <div {...dialogProps} ref={ref} className="p-4">
      {title && (
        <h3 {...titleProps} className="mt-0">
          {title}
        </h3>
      )}
      {children}
    </div>
  )
}

export default Dialog
