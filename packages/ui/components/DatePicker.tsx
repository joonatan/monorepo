import React, { type RefObject, useRef } from 'react'
import { useDatePicker, type AriaDatePickerProps } from 'react-aria'
import { useDatePickerState } from 'react-stately'
import { type DateValue } from '@internationalized/date'
import { FaCalendarAlt as CalendarIcon } from 'react-icons/fa'
import { FaExclamation as ExclamationIcon } from 'react-icons/fa'

// Reuse the DateField, Popover, Dialog, Calendar, and Button from your component library.
import { Button, Calendar, DateField, Dialog, Popover } from './'
import { FieldButton } from './Button'
import { twMerge } from 'tailwind-merge'

type Props = {
  className?: string
} & AriaDatePickerProps<DateValue>
// TODO allow resrticing the date range available
// TODO render the year selector inside the calendar (allow toggling it on / off)
// TODO this should not use button styling (rounded)
function DatePicker(props: Props) {
  const { className, ...rest } = props
  const state = useDatePickerState(rest)
  const ref: RefObject<HTMLDivElement> = useRef<HTMLDivElement>(null)
  const {
    groupProps,
    labelProps,
    fieldProps,
    buttonProps,
    dialogProps,
    calendarProps,
  } = useDatePicker(rest, state, ref)

  // TODO find a decent calendar icon (non colored, flat design)
  return (
    <div
      className={twMerge('relative inline-flex flex-col text-left', className)}
    >
      <span {...labelProps} className="text-sm text-gray-800">
        {props.label}
      </span>
      <div {...groupProps} ref={ref} className="group flex">
        <div className="relative flex flex-row items-center gap-4 rounded-l-md border border-gray-300 bg-white p-1 pr-10 transition-colors group-focus-within:border-violet-600 group-hover:border-gray-400 group-focus-within:group-hover:border-violet-600">
          <DateField {...fieldProps} />
          {state.validationState === 'invalid' && (
            <ExclamationIcon className="absolute right-1 h-6 w-6 text-red-500" />
          )}
          <FieldButton {...buttonProps}>
            <CalendarIcon className="h-5 w-5 text-gray-700 group-focus-within:text-violet-700" />
          </FieldButton>
        </div>
      </div>
      {state.isOpen && (
        <Popover state={state} triggerRef={ref} placement="bottom start">
          <Dialog title="" {...dialogProps}>
            <Calendar {...calendarProps} />
          </Dialog>
        </Popover>
      )}
    </div>
  )
}

export default DatePicker
