import React, { type RefObject, useRef } from 'react'
import { useDateFieldState } from 'react-stately'
import { type AriaDatePickerProps, useDateField, useLocale } from 'react-aria'
import { createCalendar } from '@internationalized/date'
import { type DateValue } from '@internationalized/date'
import DateSegment from './DateSegment'

function DateField(props: AriaDatePickerProps<DateValue>) {
  const { locale } = useLocale()
  const state = useDateFieldState({
    ...props,
    locale,
    createCalendar,
  })

  const ref: RefObject<HTMLDivElement> = useRef<HTMLDivElement>(null)
  const { labelProps, fieldProps } = useDateField(props, state, ref)

  return (
    <div className="flex">
      <span {...labelProps}>{props.label}</span>
      <div {...fieldProps} ref={ref} className="flex flex-row">
        {state.segments.map((segment, i) => (
          <DateSegment key={i} segment={segment} state={state} />
        ))}
        {state.validationState === 'invalid' && (
          <span aria-hidden="true">🚫</span>
        )}
      </div>
    </div>
  )
}

export default DateField
