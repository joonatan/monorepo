import React, { type HTMLProps, type CSSProperties, forwardRef } from 'react'

type Props = {
  label: string
  className?: string
  style?: CSSProperties
  error?: string
} & HTMLProps<HTMLInputElement>

type Ref = HTMLInputElement

// TODO required?
const TextInput = forwardRef<Ref, Props>(function TextInput(props, ref) {
  const { label, className, style, name, error, required, ...field } = props

  return (
    <label className={className} style={style}>
      <div className="mb-1">{`${label} ${required ? ' *' : ''}`}</div>
      <input
        {...field}
        ref={ref}
        name={name}
        required={required}
        placeholder={name}
        className="w-full rounded-md p-2 text-slate-800"
        type="text"
      />
      {error != null && <span className="text-sm text-red-400">{error}</span>}
    </label>
  )
})

export default TextInput
