import type { RefObject } from 'react'
import React, { useRef } from 'react'
import {
  type AriaButtonProps,
  useButton,
  useFocusRing,
  mergeProps,
} from 'react-aria'
import { twMerge } from 'tailwind-merge'

// className will override any other styling i.e. using it for bg- will disable primary / disabled styling
// since those use bg- as part of the style.
// it's ment for fast protyping or small changes (like small / icon buttons or buttons without borders).
function Button(
  props: AriaButtonProps & { primary?: boolean; className?: string }
) {
  const ref: RefObject<HTMLButtonElement> = useRef<HTMLButtonElement>(null)
  const { buttonProps } = useButton(props, ref)
  const { focusProps, isFocusVisible } = useFocusRing()
  const { className, primary, isDisabled, children } = props
  return (
    <button
      {...mergeProps(buttonProps, focusProps)}
      ref={ref}
      className={twMerge(
        `rounded-full p-2 px-8 ${isDisabled ? 'text-gray-400' : ''} ${
          !isDisabled
            ? 'hover:bg-violet-200 active:bg-violet-300 dark:hover:bg-violet-700 dark:active:bg-violet-900'
            : ''
        } outline-none ${
          isFocusVisible
            ? 'ring-2 ring-purple-600 ring-offset-2 dark:ring-purple-900'
            : ''
        }
      ${
        primary
          ? 'bg-purple-200 dark:bg-purple-700'
          : 'border border-purple-300 dark:border-purple-700'
      }
      `,
        className
      )}
    >
      {children}
    </button>
  )
}

// TODO do we need a isPressed
export function FieldButton(props: AriaButtonProps) {
  const ref: RefObject<HTMLButtonElement> = useRef<HTMLButtonElement>(null)
  const { buttonProps, isPressed } = useButton(props, ref)
  return (
    <button
      {...buttonProps}
      ref={ref}
      className={`-ml-px rounded-r-md border px-2 outline-none transition-colors group-focus-within:border-violet-600 group-focus-within:group-hover:border-violet-600 ${
        isPressed
          ? 'border-gray-400 bg-gray-200'
          : 'border-gray-300 bg-gray-50 group-hover:border-gray-400'
      }`}
    >
      {props.children}
    </button>
  )
}

export default Button
