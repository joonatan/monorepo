import { forwardRef, type CSSProperties, useRef } from 'react'
import {
  type AriaTimeFieldProps,
  useTimeField,
  useDateSegment,
} from 'react-aria'
import { useObjectRef } from '@react-aria/utils'
import { type DateFieldState, useTimeFieldState } from 'react-stately'
import { type DateSegment } from '@react-stately/datepicker'
import { type Time } from '@internationalized/date'

// Note: this component is the same as in the useDateField docs.
function DateSegmentComp({
  segment,
  state,
}: {
  segment: DateSegment
  state: DateFieldState
}) {
  const ref = useRef(null)
  const { segmentProps } = useDateSegment(segment, state, ref)

  return (
    <div
      {...segmentProps}
      ref={ref}
      // className={`segment ${segment.isPlaceholder ? 'placeholder' : ''}`}
      className={`${!segment.isEditable ? 'text-slate-500' : 'text-slate-800'}`}
    >
      {segment.text}
    </div>
  )
}

type Props = {
  label: string
  className?: string
  style?: CSSProperties
} & AriaTimeFieldProps<Time>

// TODO do we need / want error
// TODO required should be displayed
const TimeInput = forwardRef<HTMLDivElement, Props>(function Input(
  props,
  forwardedRef
) {
  const { className, style, ...rest } = props
  // NOTE hard code locale to use 24h clock like a regular person
  const locale = 'fi'
  const state = useTimeFieldState({
    ...rest,
    locale,
  })
  const ref = useObjectRef<HTMLDivElement>(forwardedRef)

  const { labelProps, fieldProps } = useTimeField(rest, state, ref)

  return (
    <div className={className} style={style}>
      <span {...labelProps}>{props.label}</span>
      <div {...fieldProps} ref={ref} className="flex rounded-md bg-white p-2">
        {state.segments.map((segment, i) => (
          <DateSegmentComp key={i} segment={segment} state={state} />
        ))}
        {state.validationState === 'invalid' && (
          <span aria-hidden="true">🚫</span>
        )}
      </div>
    </div>
  )
})

export default TimeInput
