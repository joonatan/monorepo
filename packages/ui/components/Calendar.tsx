import React, { type RefObject, useRef } from 'react'
import {
  useCalendar,
  useCalendarCell,
  useCalendarGrid,
  useLocale,
  useFocusRing,
  mergeProps,
  type CalendarProps,
} from 'react-aria'
import {
  type CalendarState,
  type RangeCalendarState,
  useCalendarState,
} from 'react-stately'
import {
  createCalendar,
  getWeeksInMonth,
  type DateValue,
  type CalendarDate,
} from '@internationalized/date'

import { Button } from './'

function Calendar(props: CalendarProps<DateValue> & { className?: string }) {
  const { locale } = useLocale()
  const state = useCalendarState({
    ...props,
    locale,
    createCalendar,
  })

  const ref: RefObject<HTMLDivElement> = useRef<HTMLDivElement>(null)
  const { calendarProps, prevButtonProps, nextButtonProps, title } =
    useCalendar(props, state)

  return (
    <div
      {...calendarProps}
      ref={ref}
      className={`${props.className} inline-block rounded-xl bg-neutral-300 text-slate-800 dark:bg-slate-800 dark:text-slate-300`}
    >
      <div className="flex items-center px-2 pb-4">
        <h2 className="ml-2 flex-1 text-xl font-bold">{title}</h2>
        <Button className="border-none px-2" {...prevButtonProps}>
          &lt;
        </Button>
        <Button className="border-none px-2" {...nextButtonProps}>
          &gt;
        </Button>
      </div>
      <CalendarGrid state={state} />
    </div>
  )
}

function CalendarGrid({
  state,
  ...props
}: {
  state: CalendarState | RangeCalendarState
}) {
  const { locale } = useLocale()
  const { gridProps, headerProps, weekDays } = useCalendarGrid(props, state)

  // Get the number of weeks in the month so we can render the proper number of rows.
  const weeksInMonth = getWeeksInMonth(state.visibleRange.start, locale)

  return (
    <table {...gridProps} cellPadding="0" className="flex-1">
      <thead {...headerProps} className="text-slate-600">
        <tr>
          {weekDays.map((day, index) => (
            <th className="text-center" key={index}>
              {day}
            </th>
          ))}
        </tr>
      </thead>
      <tbody>
        {[...new Array(weeksInMonth).keys()].map((weekIndex) => (
          <tr key={weekIndex}>
            {state
              .getDatesInWeek(weekIndex)
              .map((date, i) =>
                date ? (
                  <CalendarCell key={i} state={state} date={date} />
                ) : (
                  <td key={i} />
                )
              )}
          </tr>
        ))}
      </tbody>
    </table>
  )
}

function CalendarCell({
  state,
  date,
}: {
  state: CalendarState | RangeCalendarState
  date: CalendarDate
}) {
  const ref: RefObject<HTMLDivElement> = useRef<HTMLDivElement>(null)
  const {
    cellProps,
    buttonProps,
    isSelected,
    isOutsideVisibleRange,
    isDisabled,
    formattedDate,
    isInvalid,
    isUnavailable,
  } = useCalendarCell({ date }, state, ref)

  // The start and end date of the selected range will have
  // an emphasized appearance.

  const isRoundedLeft = isSelected
  const isRoundedRight = isSelected

  const { focusProps, isFocusVisible } = useFocusRing()

  return (
    <td
      {...cellProps}
      className={`relative py-0.5 ${isFocusVisible ? 'z-10' : 'z-0'}
      `}
    >
      <div
        {...mergeProps(buttonProps, focusProps)}
        ref={ref}
        hidden={isOutsideVisibleRange}
        className={`group h-10 w-10 outline-none ${
          isRoundedLeft ? 'rounded-l-full' : ''
        } ${isRoundedRight ? 'rounded-r-full' : ''} ${
          isSelected
            ? isInvalid
              ? 'bg-red-300'
              : 'bg-violet-300 dark:bg-violet-900'
            : ''
        } ${isUnavailable ? 'bg-neutral-200 text-slate-600' : ''}`}
      >
        <div
          className={`flex h-full w-full items-center justify-center rounded-full ${
            isDisabled ? 'text-slate-400' : ''
          } ${
            // Focus ring, visible while the cell has keyboard focus.
            isFocusVisible
              ? 'ring-2 ring-violet-600 ring-offset-2 group-focus:z-10 dark:ring-violet-900'
              : ''
          }
          ${
            // Hover state for non-selected cells.
            !isSelected && !isDisabled
              ? 'hover:bg-violet-200 dark:hover:bg-violet-800'
              : ''
          } cursor-default`}
        >
          {formattedDate}
        </div>
      </div>
    </td>
  )
}

export default Calendar
