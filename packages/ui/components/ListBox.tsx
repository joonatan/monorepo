import type { ListState, Node } from 'react-stately'
import { useListState } from 'react-stately'
import {
  mergeProps,
  useFocusRing,
  useListBox,
  useOption,
  type AriaListBoxOptions,
} from 'react-aria'
import React, { type RefObject, useRef } from 'react'

// TODO this needs a refactor (what is it's purpose)
export default function ListBox(
  props: AriaListBoxOptions<object> & { children: JSX.Element | JSX.Element[] }
) {
  // Create state based on the incoming props
  const state = useListState(props)

  // Get props for the listbox element
  const ref: RefObject<HTMLUListElement> = useRef<HTMLUListElement>(null)
  const { listBoxProps, labelProps } = useListBox(props, state, ref)

  return (
    <>
      <div {...labelProps}>{props.label}</div>
      <ul
        {...listBoxProps}
        ref={ref}
        className="mx-2 my-0 cursor-pointer overflow-hidden border border-gray-700 p-0"
      >
        {[...state.collection].map((item) =>
          item.type === 'section' ? (
            <div key={item.key}>ERROR: ListBoxSections not supported</div>
          ) : (
            <Option key={item.key} item={item} state={state} />
          )
        )}
      </ul>
    </>
  )
}

function Option<T>({
  item,
  state,
}: {
  item: Node<T>
  state: ListState<unknown>
}) {
  // Get props for the option element
  const ref = React.useRef<HTMLLIElement>(null)
  const { optionProps, isSelected, isDisabled } = useOption(
    { key: item.key },
    state,
    ref
  )

  // Determine whether we should show a keyboard
  // focus ring for accessibility
  const { isFocusVisible, focusProps } = useFocusRing()

  return (
    <li
      {...mergeProps(optionProps, focusProps)}
      ref={ref}
      style={{
        background: isSelected ? 'blueviolet' : 'transparent',
        color: isDisabled ? '#aaa' : isSelected ? 'white' : undefined,
        padding: '2px 5px',
        outline: isFocusVisible ? '2px solid orange' : 'none',
      }}
    >
      {item.rendered}
    </li>
  )
}
