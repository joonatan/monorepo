import { z } from 'zod'

export const TimeSchema = z.object({
  hour: z.number(),
  minute: z.number(),
  second: z.number(),
  millisecond: z.number(),
})

export const DateSchema = z.object({
  calendar: z.unknown(), // { indentifier: gregory }
  era: z.string(), // AD probably
  // assuming Gregorian calendar
  month: z.number().min(1).max(12),
  day: z.number().min(1).max(31),
  year: z.number(),
})
