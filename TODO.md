# Monorepo: TODO

- DONE: Setup turbopack?
- DONE: migrate the don't break the chain (T3 stack)
- DONE: cleanup the don't-break-the-chain source code (left over components / styles.css)
  Move components to ui/
- DONE: two prisma database connections mess the PrismaClients
  - so our LSP and TS checks are broken (works in runtime)
  - could try moving to two separate database apps to import the correct one in each
  - WORKS: for chains at least (some problems with the LSP still)
- DONE: configure prisma generate dirs because the clients are conflicting with each other)
- TODO: add errors and required to DatePicker and TimeInput components
- TODO: DatePicker should show Monday first
- TODO: styling DatePicker (extra stuff => remove)
- TODO: styling button (especially light variant looks awful, hover effects etc.)
- TODO: add server side data fetching for Reservations
- TODO: add meta to Reservations (can we make it so that it fetches it with SSR props but the component is in app.tsx?)
- TODO: add i18n
- TODO: try moving the environment variables to root (prisma is probably gonna hate us)
- TODO: add envs to turbo configuration (causing lint errors at moment)
- FIX: db:seed and db:push --force-reset commands need to be usable from project root
  - at the moment they only work in the database package directory and require envs to be in the same directory
  - both projects need their own version of it
- Add DateSelector component with custom Calendar drawing
- dont-break-the-chain: Fix form submissions and validation of DateInput
- Add Discord auth to dont-break-the-chain
- FIX: app env variables need to be in turbo.json
- TODO: add a mobile app (using React native)
- TODO: add a swift app for Apple
- TODO: add TS library that is both shared and exported from this repo (tsup)
  - publish it as an NPM just for funzies

- Add PlanetScale DB
  - dont-break-the-chain
  - reservations
    Need to create a single common database for my small projects
- Deploy dont-break-the-chain to Vercel

- migrate image generator (NextJs / Astro / api)
- Setup Vercel deployments
  - each app to their separate endpoint
- migrate components to the common ui library
- migrate my blog (Astro without Keystone backend)
- Add a SolidStart project (create jd-app)
- migrate the Keystone CMS backend for blog (need persistent database for it also like PlanetScale)
- Rename the ui => to react-ui (so we can add astro-ui and solid-ui for those components)
- Setup Vercel functions for the image generator API
  - requires rewriting it with NextJs (instead of fastify)
- Setup a basic JSON backend with Rust
  - deploy to Vercel with: https://github.com/vercel-community/rust
- Write some basic component tests for react
- Add playwrite for e2e testing
- Setup husky (lint + tests)

Add a separate project for testing out Next13 App directory.
  What should it do?
  No tRPC / Prisma for now (doing the setup manually)

Add a separate SolidStart project.

TODO: add react-hook-form and create input components for it in ui/components

TODO: tailwind config is scetchy but at least I got it working
see if we can remove some of the postcss.config.cjs files.
see if we can make the path in tailwind-config not relative
see if we can make custom theming changes in different files (i.e. next app vs. ui)
